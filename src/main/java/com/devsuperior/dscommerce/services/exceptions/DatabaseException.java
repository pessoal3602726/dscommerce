package com.devsuperior.dscommerce.services.exceptions;

public class DatabaseException extends RuntimeException {
	
	private static final long serialVersionUID = 3310752268785655244L;

	public DatabaseException(String msg) {
		super(msg);
	}
	
}
