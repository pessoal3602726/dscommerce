package com.devsuperior.dscommerce.services.exceptions;

public class ResourceNotFoundException extends RuntimeException{
	
	private static final long serialVersionUID = 3529889871711286778L;

	public ResourceNotFoundException(String msg) {
		super(msg);
	}

}
